# -*- coding: utf-8 -*-
# This file is part of health_cepsap_icd10 module for Tryton.
# The COPYRIGHT file at the top level of this repository contains
# the full copyright notices and license terms.

from trytond.pool import Pool
from . import health


def register():
    Pool.register(
        health.PathologyCategoryConfiguration,
        health.PathologyCategory,
        health.Pathology,
        health.PathologyGroup,
        health.DiseaseMembers,
        health.PrescriptionLine,
        health.PatientEvaluation,
        module='health_cepsap_icd10', type_='model')
